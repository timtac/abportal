﻿using ExpressiveAnnotations.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EssPortal.Models
{
    public class LeaveApplicationViewModel : LeaveInformation
    {
        #region ------------ Properties -----------------------

        [MaxLength(20, ErrorMessage = "Maximum of 20 characters allowed.")]
        public string ApplicationNo { get; set; }

        public string ApplicationDate { get; set; }

        [Required]
        [Display(Name = "Employee No.")]
        public string EmployeeNo { get; set; }

        [Display(Name = "Employee Name")]
        public string EmployeeName { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        public string Branch { get; set; }

        public string Department { get; set; }

        [Display(Name = "Supervisor Name(for Approval)")]
        public string SupervisorName { get; set; }

        [Display(Name = "Supervisor E-Mail")]
        public string SupervisorEmail { get; set; }

        [Required]
        [Display(Name = "Leave Type")]
        public string LeaveType { get; set; }

        [Required]
        [Display(Name = "Leave Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string LeaveStartDate { get; set; }

        [Required]
        [Display(Name = "Leave End Date")]
        [DisplayFormat(ApplyFormatInEditMode =true,DataFormatString = "{0:dd/MM/yyyy}")]
        public string LeaveEndDate { get; set; }

        // To be autoopulated.
        [Range(0.0, 365.0, ErrorMessage = "Enter a value less than the allowed leave balance.")]
        [StringLength(4, ErrorMessage = "Maximum of 4 numeric characters.")]
        [Display(Name = "No. of Leave Days Applied for")]
        public string LeaveDaysAppliedFor { get; set; }

        [Required]
        [Range(0.0, 365.0, ErrorMessage = "Enter a value less than the appllied leave days.")]
        [StringLength(4, ErrorMessage = "Maximum of 4 numeric characters.")]
        [Display(Name = "Approved Leave Days")]
        public string LeaveDaysApproved { get; set; }

        [StringLength(4, ErrorMessage = "Maximum of 4 numeric characters.")]
        [Display(Name = "Total Annual Leave Days")]
        public string TotalAnnualLeaveDays { get; set; }

        [Display(Name = "Start Date of Approved Leave")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string StartDateOfApprovedLeave { get; set; }

        [Display(Name = "End Date of Approved Leave")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string EndDateOfApprovedLeave { get; set; }

        [StringLength(4, ErrorMessage = "Maximum of 4 numeric characters.")]
        [Display(Name = "Total Annual Leave Days Taken")]
        public string TotalAnnualLeaveTaken { get; set; }

        [StringLength(4, ErrorMessage = "Maximum of 4 numeric characters.")]
        [Display(Name = "Annual Balance(Days)")]
        public string TotalAnnualLeaveBalance { get; set; }

        [Display(Name = "Annual Leave Days Utilized")]
        public string AnnualLeaveDaysUtilized { get; set; }

        [Required(ErrorMessage ="A reliever code is required.")]
        [Display(Name = "Reliever Name")]
        public string RelieverCode { get; set; }

        [Display(Name = "To Include Half Day")]
        public bool ToIncludeHalfDay { get; set; }

        [Display(Name = "Reliever Name")]
        public string RelieverName { get; set; }

        [Display(Name = "Application Status")]
        public string LeaveApplicationStatus { get; set; }

        [Required]
        [Display(Name = "Rejection Reason")]
        public string RejectionReason { get; set; }

        // used to control certain parts of the page to make editable or not. eg. Rejection Reason and Leave days approved.
        public bool IsSupervisor { get; set; }

        // This fields will be hidden and used to inform the nav wether to save the leave application or send it for approaval.
        // Saving means setting the status flag to NEW while sending it for approval will set the status flag to PENDING APPROVAL.
        public bool SubmitLeave { get; set; }

        public bool Approved { get; set; }

        // the date from which an employee can apply for leave.
        public string ValidLeaveDateFrom { get; set; }
        public int leaveMonths { get; set; }

        public IEnumerable<SelectListItem> Employees { set; get; }

        #endregion --------------------------------------------

    }
}