﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EssPortal.Models
{
    public class IndexViewModel : LeaveInformation
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
        public bool IsEmployee { get; set; }

        public List<JobApplied> JobsApplied { get; set; }
        public ApplicantBioData Biodata { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }


    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }

    // Contains part biodata properties with the same validation rules accross different usage.
    public class UserBiodata
    {
        [Display(Name = "Title *")]
        public string UserTitle { get; set; }

        [Display(Name = "First Name *")]
        [MaxLength(80, ErrorMessage = "Maximum of 80 characters allowed.")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters allowed.")]
        public string MiddleName { get; set; }

        [Display(Name = "Last Name *")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters allowed.")]
        public string LastName { get; set; }

        [Display(Name = "Cell Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^[0-9]{1,25}$", ErrorMessage = "Only digits allowed.")]
        public string CellPhoneNo { get; set; }

        [Display(Name = "Home Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^[0-9]{1,25}$", ErrorMessage = "Only digits allowed.")]
        public string HomePhoneNumber { get; set; }

        [Display(Name = "Postal Address")]
        [MaxLength(250, ErrorMessage = "Maximum of 250 characters allowed.")]
        public string PostalAddress { get; set; }

        [Display(Name = "Residential Address (Street, No.)")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters allowed.")]
        public string ResidentialAddressStreet { get; set; }

        [Display(Name = "Residential Address (City)")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters allowed.")]
        public string ResidentialAddressCity { get; set; }

        [Display(Name = "Residential Address (District)")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters allowed.")]
        public string ResidentialAddressDistrict { get; set; }

        [Display(Name = "Private E-Mail")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Country { get; set; }
    }

    public class EmployeeBiodataViewModel : UserBiodata
    {
        public string EmployeeNo { get; set; }

        [Required]
        [Display(Name = "Residential Address (District) *")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters allowed.")]
        new public string ResidentialAddressDistrict { get; set; }

        public IEnumerable<System.Web.Mvc.SelectListItem> CountriesSelectListItems
        {
            get;
            set;
        }
    }
}