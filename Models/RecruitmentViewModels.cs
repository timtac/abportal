﻿using EssPortal.ApplicationAttachmentRef;
using EssPortal.Classes;
using EssPortal.JobApplicationEmpHistoryRef;
using EssPortal.JobApplicationQualificationRef;
using EssPortal.JobApplicationRef;
using EssPortal.JobApplicationRefereeRef;
using ExpressiveAnnotations.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EssPortal.Models
{
    public class JobApplicationDetail : RequestResponse
    {
        public JobApplication JobApplication { get; set; }
        public List<JobApplicationReferee> JobApplicationReferees { get; set; }
        public List<JobApplicationQualification> JobApplicationQualifications { get; set; }
        public List<JobApplicationEmpHistory> JobApplicationEmpHistorys { get; set; }
        public List<ApplicationAttachment> ApplicationAttachments { get; set; }

        public JobApplicationDetail()
        {
            JobApplication = new JobApplication();
            JobApplicationEmpHistorys = new List<JobApplicationEmpHistory>();
            JobApplicationQualifications = new List<JobApplicationQualification>();
            JobApplicationReferees = new List<JobApplicationReferee>();
            ApplicationAttachments = new List<ApplicationAttachment>();
        }
    }

    public class ApplyViewModel
    {
        public ApplicantBioData ApplicantBioData { get; set; }
        public List<ApplicantReferee> Referees { get; set; }
        public List<ApplicantQualification> Qualifications { get; set; }
        public List<ApplicantEmploymentHistory> EmploymentHistory { get; set; }
        public List<ApplicantLanguage> Languages { get; set; }

        // If true, the current information passed with the form will be used to update the user boidata.
        [Display(Name = "Check  this box to update your profile information with the infomraion")]
        public bool UpdateBiodata { get; set; }

        public string OtherDocument { get; set; }

        public List<string> DocumentTypes = new List<string>()
        {
            "Cover Letter",
            "Motivation Letter",
            "Reference Letter",
            "Certificate"
        };

        public IEnumerable<SelectListItem> OtherDocumentSelectList
        {
            get
            {
                for (int i = 0; i < DocumentTypes.Count; i++)
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = DocumentTypes[i];
                    selectListItem.Value = i.ToString();
                    selectListItem.Selected = OtherDocument == DocumentTypes[i];
                    yield return selectListItem;
                }
            }
        }

        public List<bool> FileAttached { get; set; } // new List<bool> { false, false, false };

        // The number of the job being applied for on the main system.
        public string JobApplicationNo { get; set; }
        public string JobPosition { get; set; }

        public ApplyViewModel()
        {
            ApplicantBioData = new ApplicantBioData();
            Referees = new List<ApplicantReferee>();
            Qualifications = new List<ApplicantQualification>();
            EmploymentHistory = new List<ApplicantEmploymentHistory>();
            Languages = new List<ApplicantLanguage>();
            FileAttached = new List<bool>();
            UpdateBiodata = false;
        }
    }

    public class TrackDeleted
    {
        public bool IsDeleted { get; set; }
    }

    public class ApplicantBioData :  UserBiodata
    {
        [RequiredIf("IsEmployee == false", ErrorMessage = "Title is required")]
        [Display(Name = "Title *")]
        new public string UserTitle { get; set; }

        [Display(Name = "First Name *")]
        [RequiredIf("IsEmployee == false", ErrorMessage = "First Name is required")]
        [MaxLength(80, ErrorMessage ="Maximum of 80 characters allowed.")]
        new public string FirstName { get; set; }

        [RequiredIf("IsEmployee == false", ErrorMessage = "Last Name is required")]
        [Display(Name = "Last Name *")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters allowed.")]
        new public string LastName { get; set; }

        [RequiredIf("IsEmployee == false", ErrorMessage = "ID Number is required")]
        [Display(Name = "ID Number *")]
        [MaxLength(30, ErrorMessage = "Maximum of 30 characters allowed.")]
        public string IdNo { get; set; }

        [RequiredIf("IsEmployee == false", ErrorMessage = "Citizenship is required")]
        [Display(Name = "Citizenship *")]
        public string Citizenship { get; set; }

        public string Gender { get; set; }

        [Display(Name = "Date Of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Marital Status")]
        public string MaritalStatus { get; set; }

        [Range(0.00, 1000000000.00, ErrorMessage = "Value should be in the range 0-1000000000")]
        [Display(Name = "Annual Salary Expectation")]
        public string AskingBasicPay { get; set; }

        public string Language { get; set; }

        // Job applied to.
        public string JobApplicationNo { get; set; }
        public DateTime DateApplied { get; set; }
        public Applicant_Type ApplicantType { get; set; }
        public string JobRequestNo { get; set; }
        public string PositionAppliedForCode { get; set; }

        [Display(Name = "Employee No.")]
        public string EmployeeNo { get; set; }

        [Display(Name = "Job Title")]
        public string EmployeeJobTitle { get; set; }

        public bool IsEmployee { get; set; }

        public string PrivateMailUpdate { get; set; }

        public string Age { get; set; }

        [Display(Name = "Ethnic Origin")]
        public string EthnicOrigin { get; set; }

        [Display(Name = "Work Phone No")] //[0-9]{1,25}
        [RegularExpression(@"^[0-9]{1,25}$", ErrorMessage = "Only digits allowed.")]
        public string WorkPhoneNo { get; set; }

        [Display(Name = "E-Mail")]
        [RequiredIf("IsEmployee == false", ErrorMessage = "E-Mail is required")]
        [DataType(DataType.EmailAddress)]
        new public string Email { get; set; }

        // For display purpose on the employee profile only.
        [Display(Name = "Company E-Mail")]
        public string CompanyEmail { get; set; }

        public IEnumerable<SelectListItem> GenderSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Gender type in Enum.GetValues(typeof(JobApplicationRef.Gender)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    var t = type.ToString();
                    selectListItem.Text = t == "_blank_" ? "Select Gender ..." : t;
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = Gender == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        public IEnumerable<SelectListItem> MaritalStatusSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Marital_Status type in Enum.GetValues(typeof(JobApplicationRef.Marital_Status)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    var t = type.ToString();
                    selectListItem.Text = t == "_blank_" ? "Select Marital Status ..." : t;
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = MaritalStatus == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        public IEnumerable<SelectListItem> TitleSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Title type in Enum.GetValues(typeof(JobApplicationRef.Title)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    if (type.ToString() == "_blank_")
                    {
                        selectListItem.Text = "Select Title ...";
                        selectListItem.Value = "";
                    }
                    else
                    {
                        selectListItem.Text = type.ToString();
                        selectListItem.Value = type.ToString();
                    }                    
                    selectListItem.Selected = this.UserTitle == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        public IEnumerable<SelectListItem> EthnicOriginSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Ethnic_Origin type in Enum.GetValues(typeof(JobApplicationRef.Ethnic_Origin)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = type.ToString();
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = EthnicOrigin == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        public IEnumerable<SelectListItem> CountriesSelectListItems
        {
            get;
            set;
        }
    }

    public class ApplicantReferee  : TrackDeleted
    {
        // Use internally to track the application the record is attached to.
        [MaxLength(20, ErrorMessage = "Maximum of 20 characters allowed.")]
        public string ApplicationNo { get; set; }

        [MaxLength(100, ErrorMessage = "Maximum of 100 characters allowed.")]
        public string Designation { get; set; }

        [MaxLength(200, ErrorMessage = "Maximum of 200 characters allowed.")]
        public string Names { get; set; }

        [MaxLength(100, ErrorMessage = "Maximum of 100 characters allowed.")]
        public string Institution { get; set; }

        [MaxLength(200, ErrorMessage = "Maximum of 200 characters allowed.")]
        public string Address { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [RegularExpression(@"^[0-9]{1,25}$", ErrorMessage = "Only digits allowed.")]
        public string TelephoneNo { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "E-Mail")]
        [MaxLength(100, ErrorMessage = "Maximum of 100 characters allowed.")]
        public string EMail { get; set; }

        public IEnumerable<SelectListItem> TitleSelectListItems
        {
            get
            {
                foreach (JobApplicationRef.Title type in Enum.GetValues(typeof(JobApplicationRef.Title)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = type.ToString();
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = this.Designation == type.ToString();
                    yield return selectListItem;
                }
            }
        }
    }

    public class ApplicantLanguage
    {
        public string Language { get; set; }

        public IEnumerable<SelectListItem> LanguagesSelectList
        { get; set; }
    }

    public class ApplicantQualification : TrackDeleted
    {
        // Use internally to track the application the record is attached to.
        [MaxLength(10, ErrorMessage = "Maximum of 10 characters allowed.")]
        public string ApplicationNo { get; set; }

        [Required]
        [Display(Name = "Qualification Category *")]
        public string QualificationType { get; set; }

        [Required]
        [Display(Name = "Qualification Specialization *")]
        public string QualificationCode { get; set; }

        [Required]
        [Display(Name = "Institution Awarded *")]
        [MaxLength(100, ErrorMessage = "Maximum of 100 characters allowed.")]
        public string Institution { get; set; }

        [Display(Name = "From Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string FromDate { get; set; }

        [Display(Name = "To Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string ToDate { get; set; }

        [Display(Name = "Class of Degree (e.g. Distinction, Pass etc.)")]
        public string Score { get; set; }

        //[Required]
        [Display(Name = "Any other detail")]
        [MaxLength(200, ErrorMessage = "Maximum of 200 characters allowed.")]
        public string Comment { get; set; }

        
        [Display(Name = "Any other detail")]
        [MaxLength(150, ErrorMessage = "Maximum of 150 characters allowed.")]
        public string Detail { get; set; }

        public IEnumerable<SelectListItem> QualificationTypeSelectList
        {
            get;
            set;
        }

        public IEnumerable<SelectListItem> RelatedQualificationCodes
        {
            get;
            set;
        }
    }

    public class ApplicantEmploymentHistory : TrackDeleted
    {
        // Use internally to track the application the record is attached to.
        [MaxLength(20, ErrorMessage = "Maximum of 20 characters allowed.")]
        public string ApplicationNo { get; set; }

        [RequiredIf("IsEmployee == false", ErrorMessage ="Institution/Company is required")]
        [Display(Name = "Institution/Company Name *")]
        [MaxLength(150, ErrorMessage = "Maximum of 150 characters allowed.")]
        public string CompanyName { get; set; }

        [RequiredIf("IsEmployee == false", ErrorMessage = "Job Title is required")]
        [Display(Name = "Job Title *")]
        [MaxLength(150, ErrorMessage = "Maximum of 150 characters allowed.")]
        public string JobTitle { get; set; }

        [RequiredIf("IsEmployee == false", ErrorMessage = "From Date is required")]
        [Display(Name = "From Date *")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string FromDate { get; set; }

        [RequiredIf("IsEmployee == false", ErrorMessage = "To Date is required")]
        [Display(Name = "To Date *")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string ToDate { get; set; }

        [MaxLength(30, ErrorMessage = "Maximum of 30 characters allowed.")]
        public string Department { get; set; }

        [MaxLength(200, ErrorMessage = "Maximum of 200 characters allowed.")]
        public string Comment { get; set; }
        public bool IsEmployee { get; set; }
    }

    public class ApplicationAttachments
    {
        public string Type { get; set; }
    }

    // Used to pass the field index number to the template to properly name html elements in a list. ex. class[0].property
    public class HtmlFieldIndexer
    {
        public int Index { get; set; }
    }
}