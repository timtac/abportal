﻿using EssPortal.JobRequirementRef;
using EssPortal.VacancyRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssPortal.Models
{
    public class RecruitmentIndexViewModel
    {
        public List<Vacancy> Vacancies{ get; set; }
        public List<JobApplied> JobsAppliedTo { get; set; }

        public RecruitmentIndexViewModel()
        {
            Vacancies = new List<Vacancy>();
        }
    }
    public class JobRequirementViewModel : RecruitmentIndexViewModel
    {
        public IEnumerable<IGrouping<string,HR_Job_Requirement_Lines>> JobRequirement { get; set; }
        public Vacancy SelectedVacancy { get; set; }
    }

    public class VacancyViewModel
    {

    }

}