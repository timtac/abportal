﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EssPortal.Models
{
    using PerformanceMgmtRef;
    using System.ComponentModel.DataAnnotations;

    public class AppraisalIndexViewModel
    {
        public List<PerformanceMgmt> Appraisals { get; set; }
        public List<PerformanceMgmt> PendingApprovals { get; set; }
    }

    public class AppraisalDetailViewModel
    {
        public PerformanceMgmt Appraisal { get; set; }
    }

    public class AppraisalViewModel
    {
        #region -----=------ AUTO POPULATED FIELDS -----------------

        [Display(Name = "Appraisal No")]
        public string AppraisalNo { get; set; }

        [Display(Name = "Appraisal Period")]
        public string AppraisalPeriod { get; set; }

        [Display(Name = "Employee Name")]
        public string EmployeeName { get; set; }

        [Display(Name = "Employee No")]
        public string EmployeeNo { get; set; }

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Display(Name = "Branch")]
        public string EmployeeBranch { get; set; }

        [Display(Name = "Date Of Joining")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string DateOfJoining { get; set; }

        [Display(Name = "Date Of Last Appraisal")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string DateOfLastAppraisal { get; set; }

        [Display(Name = "Appraiser Name")]
        public string AppraiserName { get; set; }

        [Display(Name = "Appraiser Job Title")]
        public string AppraiserJobTitle { get; set; }

        [Display(Name = "Appraisal Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string AppraisalDate { get; set; }

        [Display(Name = "Appraisal Talk Took Place on")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string DateOfAgreement { get; set; }

        [Display(Name = "Appraisal Talk Took Place")]
        public string AppraisalTalkTookPlace { get; set; }

        public IEnumerable<SelectListItem> AppraisalTookPlaceSelectList
        {
            get
            {
                foreach (Appraisal_Talk_Took_Place type in Enum.GetValues(typeof(Appraisal_Talk_Took_Place)))
                {
                    SelectListItem selectListItem = new SelectListItem();
                    selectListItem.Text = type.ToString().ToLower() == "_blank_" ? "" : type.ToString();
                    selectListItem.Value = type.ToString();
                    selectListItem.Selected = AppraisalTalkTookPlace == type.ToString();
                    yield return selectListItem;
                }
            }
        }

        // HR Performance Management respective Card (managerial/non-managerial);(which have been agreed the previous year for the current appraisal period; should be editable and adding more tasks/ objectives should be possible)
        public string TaskObjective { get; set; }

        #endregion Auto Populated fields from FRD
        // Use to track if the submission is coming from an appraisee or an appraiser
        public bool IsAppraiser { get; set; }
        // Get selectitems list for selection of ratings.
        //PerformanceRatingSelectList
        public List<SelectListItem> Rating { get; set; }

        // Used to determine if the form is to be sent to the appraiser or saved for subsequent Update.
        public bool SendForApproval { get; set; }
        public bool ApproverSave { get; set; }

        // Used to determine if the form is to be sent to the HR or back to the appraisee for Review and correction.
        public bool SendToHr { get; set; }

        // Rating values. used to hide the rating values on the page so it can be used for calculations wihtin the page.
        public List<PerformanceRatingsRef.PerformanceRatings> DefinedRatings { get; set; }

        public List<PerformanceCriteria> PerformanceCriteriaPreCurrYear { get; set; }

        public BehaviourTemplateGroup CompetencyCriteriaBehaviour { get; set; }
        public BehaviourTemplateGroup SelfAssesementIndividualDev { get; set; }

        public List<PerformanceCriteria> ObjectiveForNextAppraisal { get; set; }
        public List<PerformanceCriteria> OpenRoomDiscussion { get; set; }
        
        // Default constructor
        public AppraisalViewModel()
        {
            DefinedRatings = new List<PerformanceRatingsRef.PerformanceRatings>();
            PerformanceCriteriaPreCurrYear = new List<EssPortal.Models.PerformanceCriteria>();
            CompetencyCriteriaBehaviour = new BehaviourTemplateGroup();
            SelfAssesementIndividualDev = new BehaviourTemplateGroup();
            ObjectiveForNextAppraisal = new List<PerformanceCriteria>();
            OpenRoomDiscussion = new List<PerformanceCriteria>();
        }
    }

    public class BehaviourTemplateGroup
    {
        public string GroupName { get; set; }
        public bool UseGrading { get; set; }
        public List<SelectListItem> Rating { get; set; }
        public List<PerformanceCriteria> GroupMembers { get; set; }

        public BehaviourTemplateGroup()
        {
            GroupMembers = new List<PerformanceCriteria>();
            Rating = new List<SelectListItem>();
        }
    }

    public class PerformanceCriteria
    {
        // The Line No of the record on NAV. Used for the purpose of updating existing records.
        public int LineNo { get; set; }

        public string Objective { get; set; }

        public string Description { get; set; }

        public string Description_2 { get; set; }

        [MaxLength(250, ErrorMessage = "Maximum of 250 characters allowed.")]
        public string SelfAssesment { get; set; }

        [MaxLength(250, ErrorMessage = "Maximum of 250 characters allowed.")]
        public string AppraiserAssessment { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(250, ErrorMessage = "Maximum of 250 characters allowed.")]
        public string AppraiseeComment { get; set; }

        [DataType(DataType.MultilineText)]
        [MaxLength(250, ErrorMessage = "Maximum of 250 characters allowed.")]
        public string AppraiserComment { get; set; }

        public decimal SelfRatingValue { get; set; }

        public decimal AppraiserRatingValue { get; set; }

        private List<SelectListItem> selfAssesmentRating;
        private List<SelectListItem> appraiserAssessmentRating;

        public List<SelectListItem> SelfAssesmentRating
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                foreach (var item in this.selfAssesmentRating)
                {
                    if (item.Value == this.SelfAssesment)
                    {
                        list.Add(new SelectListItem { Text = item.Text, Value = item.Value, Selected = true });
                    }
                    else
                    {
                        list.Add(new SelectListItem { Text = item.Text, Value = item.Value, Selected = false });
                    }
                }
                return list;
            }
            set { selfAssesmentRating = value; }
        }

        public List<SelectListItem> AppraiserAssessmentRating
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                foreach (var item in this.appraiserAssessmentRating)
                {
                   
                    if (item.Value == this.AppraiserAssessment)
                    {
                        list.Add(new SelectListItem { Text = item.Text, Value = item.Value, Selected = true });
                    }
                    else
                    {
                        list.Add(new SelectListItem { Text = item.Text, Value = item.Value, Selected = false });
                    }
                }
                return list;
            }
            set { appraiserAssessmentRating = value; }
        }
    }
}