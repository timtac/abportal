namespace EssPortal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class NavConfigTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NavConfigs",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    NavID = c.String(),
                    NavPsw = c.String(),
                })
                .PrimaryKey(t => t.Id);

        }

        public override void Down()
        {
            DropTable("dbo.NavConfigs");
        }
    }
}
