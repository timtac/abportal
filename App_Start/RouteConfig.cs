﻿using System.Web.Mvc;
using System.Web.Routing;

namespace EssPortal
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            // Leave Ruotes
            routes.MapRoute(
                name: "Leave",
                url: "leave/",
                defaults: new { controller = "Leave", action = "Index", id = UrlParameter.Optional }
            );

            //Encrypt
            routes.MapRoute(
                name: "Encrypt",
                url: "Encrypt/",
                defaults: new { controller = "Encrypt", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}