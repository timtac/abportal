﻿using System.Web.Optimization;

namespace EssPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/expressive.annotations.validate.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));


            bundles.Add(new ScriptBundle("~/bundles/themescripts").Include(
                      "~/Content/template/bower_components/jquery/dist/jquery.js",
                      "~/Content/template/bower_components/bootstrap/dist/js/bootstrap.js",
                      "~/Content/template/bower_components/metisMenu/dist/metisMenu.js",
                      "~/Content/template/bower_components/raphael/raphael.js",
                      "~/Content/bootstrap-datepicker/js/bootstrap-datepicker.js",
                      "~/Scripts/bootbox.min.js",
                      "~/Scripts/bootstrap-select.js",
                      "~/Content/template/dist/js/sb-admin-2.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));


            bundles.Add(new StyleBundle("~/Content/themecss").Include(
                      "~/Content/template/bower_components/bootstrap/dist/css/bootstrap.css",
                      "~/Content/template/bower_components/metisMenu/dist/metisMenu.css",
                      "~/Content/template/dist/css/timeline.css",
                      "~/Content/template/dist/css/sb-admin-2.css",
                      "~/Content/bootstrap-select.css",
                      //"~/Content/template/bower_components/morrisjs/morris.css",
                      "~/Content/bootstrap-datepicker/css/bootstrap-datepicker.css",
                      "~/Content/template/bower_components/font-awesome/css/font-awesome.css"));
        }
    }
}
