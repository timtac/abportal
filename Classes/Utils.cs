﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssPortal.Classes
{
    public static class Utils
    {
        public static string DATE_FORMAT = "dd/MM/yyyy";

        public static string DateDisplayString(DateTime date)
        {
            string dateDisplayString = date.ToString(DATE_FORMAT);
            // Filter out SQL Server or NAV default dates.
            if ((dateDisplayString == "01/01/0001") || (dateDisplayString == "01/01/1900"))
                dateDisplayString = "";
            return dateDisplayString;
        }
    }
}