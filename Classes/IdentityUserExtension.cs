﻿using EssPortal.Classes;
using EssPortal.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace EssPortal
{
    public static class IdentityUserExtension
    {
        static ApplicationDbContext context = ApplicationDbContext.Create();

        public static bool UserIsEmployee(this IIdentity identity)
        {
            try
            {
                string username = identity.Name;
                var isEmployee = context.Users.Where(u => u.UserName == username).Select(ie => ie.IsEmployee).First();
                return isEmployee;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool UserIsEmployee(this IIdentity identity, string username)
        {
            try
            {
                var isEmployee = context.Users.Where(u => u.UserName == username).Select(ie => ie.IsEmployee).First();
                return isEmployee;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string EmployeeNo(this IIdentity identity)
        {
            try
            {
                string username = identity.Name;
                var employeeNo = context.Users.Where(u => u.UserName == username).Select(ie => ie.EmployeeNo).First();
                return employeeNo;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        public static string EmployeeNavUserId(this IIdentity identity)
        {
            try
            {
                string username = identity.Name;
                var navUserId = context.Users.Where(u => u.UserName == username).Select(ie => ie.NAVUserId).First();
                return navUserId;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Pull Identity user information. Info used to prepopulate fields for Applicants and for display on the profile page.
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static ApplicationUser UserInformation(this IIdentity identity)
        {
            try
            {
                string username = identity.Name;
                var userInfo = context.Users.Where(u => u.UserName == username).Select(u => u).First();
                return userInfo;
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Checks if a job has already been applied to by 
        public static int JobAlreadyAppliedFor(this IIdentity identity, string JobNo)
        {
            try
            {
                string userId = identity.GetUserId();
                var numOfJobsApplied = context.JobsApplied.Where(u => u.ApplicationUserId == userId && u.JobNo == JobNo).Count();
                return numOfJobsApplied;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        // Get all jobs that the current user has applied for.
        public static List<JobApplied> JobsAppliedFor(this IIdentity identity)
        {
            try
            {
                string userId = identity.GetUserId();
                var JobsApplied = context.JobsApplied.Where(u => u.ApplicationUserId == userId).ToList();
                return JobsApplied;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static int AddJobsAppliedFor(this IIdentity identity, string JobNo, string PositionAppliedFor, string JobApplicationNo)
        {
            try
            {
                string userId = identity.GetUserId();
                JobApplied job = new JobApplied();
                job.ApplicationUserId = userId;
                job.JobNo = JobNo;
                job.PositionAppliedForCode = PositionAppliedFor;
                job.DateApplied = DateTime.Now;
                job.JobApplicationNo = JobApplicationNo;
                context.JobsApplied.Add(job);
                context.SaveChanges();

                return job.Id;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static bool UpdateUserProfile(this IIdentity identity, ApplicantBioData biodata)
        {
            try
            {
                string userId = identity.Name;
                var user = context.Users.Where(u => u.UserName == userId).First();
                user.Title = biodata.UserTitle;
                user.FirstName = biodata.FirstName;
                user.MiddleName = biodata.MiddleName;
                user.LastName = biodata.LastName;
                user.IdNo = biodata.IdNo;
                user.Citizenship = biodata.Citizenship;
                user.Gender = biodata.Gender;
                DateTime date = new DateTime();
                if (DateTime.TryParse(biodata.DateOfBirth, out date))
                {
                    user.DateOfBirth = date;
                }                
                user.MaritalStatus = biodata.MaritalStatus;
                user.CellPhoneNo = biodata.CellPhoneNo;
                user.HomePhoneNumber = biodata.HomePhoneNumber;
                //JobsApplied.Email = biodata.Email;
                user.PostalAddress = biodata.PostalAddress;
                user.AddressStreet = biodata.ResidentialAddressStreet;
                user.AddressCity = biodata.ResidentialAddressCity;
                user.AddressDistrict = biodata.ResidentialAddressDistrict;
                user.Country = biodata.Country;
                decimal abp = 0;
                Decimal.TryParse(biodata.AskingBasicPay, out abp);
                user.AskingBasicPay = abp;
                user.DateLastModified = DateTime.Now;

                context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ConfirmMail(this IIdentity identity, ApplicationUser user)
        {
            try
            {
                user.EmailConfirmed = true;
                context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Used to update the Employment history, Referee, Qualifications of the currently logged in user at the point of applying for a new job.
        public static void UpdateRecruitmentDetails(this IIdentity identity, ApplyViewModel model, NAVServiceWrapper service, ApplicationDbContext dbc)
        {
            string logFile = "UpdateRecruitmentDetails";
            try
            {
                //service.LogUtil.Logger(logFile, "Starting update of user data.");

                string userId = identity.Name;
                var user = dbc.Users.Where(u => u.UserName == userId).First();


                //service.LogUtil.Logger(logFile, "Begin Delete.");
                //string re = "\n Qual: " + user.Qualifications.Count + " \n Employ Hist: " + user.EmploymentHistories.Count + " \n Referres: " + user.Referees.Count + " \n Languages: " + user.Languages.Count;
                //service.LogUtil.Logger(logFile, "data lines to delete" + re);



                // Delete all previous entries
                var ql = user.Qualifications.ToList();
                for (int i = 0; i <  ql.Count; i++)
                {
                    dbc.Qualification.Remove(ql[i]);
                }
                var el = user.EmploymentHistories.ToList();
                for (int i = 0; i < el.Count; i++)
                {
                    dbc.EmploymentHistory.Remove(el[i]);
                }
                var rl = user.Referees.ToList();
                for (int i = 0; i < rl.Count; i++)
                {
                    dbc.Referee.Remove(rl[i]);
                }
                var lang = user.Languages.ToList();
                for (int i = 0; i < lang.Count; i++)
                {
                    dbc.Language.Remove(lang[i]);
                }

                //service.LogUtil.Logger(logFile, "End Delete.");

                //service.LogUtil.Logger(logFile, "Begin Write.");
                //string re2 = "\n Qual: " + model.Qualifications.Count + " \n Employ Hist: " + model.EmploymentHistory.Count + " \n Referres: " + model.Referees.Count + " \n Languages: " + model.Languages.Count;
                //service.LogUtil.Logger(logFile, "data lines to Write" + re2);

                foreach (var item in model.EmploymentHistory)
                {
                    EmploymentHistory hist = new EmploymentHistory()
                    {
                        ApplicationUserId = user.Id,
                        Comment = item.Comment,
                        JobTitle = item.JobTitle,
                        ToDate = service.StringToDate(item.ToDate, true),
                        FromDate = service.StringToDate(item.FromDate, true),
                        CompanyName = item.CompanyName,
                        Department = item.Department
                    };
                    dbc.EmploymentHistory.Add(hist);
                }

                // Create new entries from the incoming data.
                foreach (var item in model.Referees)
                {
                    Referee r = new Referee()
                    {
                        Address = item.Address,
                        ApplicationUserId = user.Id,
                        Designation = item.Designation,
                        Institution = item.Institution,
                        Names = item.Names,
                        TelephoneNo = item.TelephoneNo,
                        EMail = item.EMail,
                        CreationDate = DateTime.Now,
                        LastModifiedDate = DateTime.Now
                    };
                    dbc.Referee.Add(r);
                }

                foreach (var item in model.Qualifications)
                {
                    Qualification q = new Qualification()
                    {
                        ApplicationUserId = user.Id,
                        Comment = item.Comment,
                        Detail = item.Detail,
                        FromDate = service.StringToDate(item.FromDate, true),
                        ToDate = service.StringToDate(item.ToDate, true),
                        Institution = item.Institution,
                        QualificationCode = item.QualificationCode,
                        QualificationType = item.QualificationType,
                        Score = item.Score,
                        CreationDate = DateTime.Now,
                        LastModifiedDate = DateTime.Now
                    };
                    dbc.Qualification.Add(q);
                }

                foreach (var item in model.Languages)
                {
                    Language q = new Language()
                    {
                        ApplicationUserId = user.Id,
                        LanguageCode = item.Language,
                        CreationDate = DateTime.Now,
                        LastModifiedDate = DateTime.Now
                    };
                    dbc.Language.Add(q);
                }

                //service.LogUtil.Logger(logFile, "End Write.");

                dbc.SaveChanges();
            }
            catch (Exception e)
            {
                service.LogUtil.Logger(logFile, e.ToString());
            }
        }

        // Get all referee, employment istory and Qualification of the current user and attach to the current model.
        // if the user has none defined for any section, attach a single empty entry.
        public static ApplyViewModel GetRecruitmentDetail(this IIdentity identity, ApplyViewModel model, NAVServiceWrapper service, bool IsEmployee)
        {
            string userId = identity.Name;
            var user = context.Users.Where(u => u.UserName == userId).First();

            // Retrieve all related entries and attach to  the model.
            var ql = context.Qualification.Where(a => a.ApplicationUserId == user.Id).ToList();
            if (ql.Count < 1)
                model.Qualifications.Add(new ApplicantQualification());
            else
            {
                foreach (var item in ql)
                {
                    model.Qualifications.Add(new ApplicantQualification()
                    {
                        Comment = item.Comment,
                        Detail = item.Detail,
                        FromDate = item.FromDate.ToString(service.DateFormat),
                        ToDate = item.ToDate.ToString(service.DateFormat),
                        Institution = item.Institution,
                        QualificationCode = item.QualificationCode,
                        QualificationType = item.QualificationType,
                        Score = item.Score
                    });
                }
            }

            var el = context.EmploymentHistory.Where(a => a.ApplicationUserId == user.Id).ToList();
            if (el.Count < 1)
            {
                model.EmploymentHistory.Add(new ApplicantEmploymentHistory() { IsEmployee = IsEmployee });
            }
            else
            {
                foreach (var item in el)
                {
                    model.EmploymentHistory.Add(new ApplicantEmploymentHistory
                    {
                        IsEmployee = IsEmployee,
                        Comment = item.Comment,
                        CompanyName = item.CompanyName,
                        Department = item.Department,
                        FromDate = item.FromDate.ToString(service.DateFormat),
                        ToDate = item.ToDate.ToString(service.DateFormat),
                        JobTitle = item.JobTitle
                    });
                }
            }

            var rl = context.Referee.Where(a => a.ApplicationUserId == user.Id).ToList();
            if (rl.Count  < 1)
            {
                model.Referees.Add(new ApplicantReferee());
            }
            else
            {
                foreach (var item in rl)
                {
                    model.Referees.Add(new ApplicantReferee()
                    {
                        Address = item.Address,
                        Designation = item.Designation,
                        EMail = item.EMail,
                        Institution = item.Institution,
                        Names = item.Names,
                        TelephoneNo = item.TelephoneNo
                    });
                }
            }

            var lang = context.Language.Where(a => a.ApplicationUserId == user.Id).ToList();
            if (lang.Count < 1)
            {
                model.Languages.Add(new ApplicantLanguage());
            }
            else
            {
                foreach (var item in lang)
                {
                    model.Languages.Add(new ApplicantLanguage()
                    {
                         Language = item.LanguageCode
                    });
                }
            }

            return model;           
        }

        //commit NavUser config details
        public static void SaveNavConfig(NavConfigViewModel model)
        {
            TextEncryption textEncryption = new TextEncryption();
            var encryptText = textEncryption.Encrypt(model.NavPsw);

            context.NavConfig.Add(new NavConfig
            {
                NavID = model.NavUserId,
                NavPsw = encryptText
            });
            context.SaveChanges();
        }

        public static NavConfigViewModel GetNavConfig()
        {
            NavConfigViewModel model = new NavConfigViewModel();
            try {
            var navConfig = context.NavConfig.Where(x => x.Id == 1).First();
            model.NavUserId = navConfig.NavID;
            model.NavPsw = navConfig.NavPsw;
            return model;
            } catch (Exception ex) {// if database isEmpty return empty model
                return model;
            }
            
        }
    }
}