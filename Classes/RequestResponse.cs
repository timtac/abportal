﻿using EssPortal.EmployeeRef;
using EssPortal.LeaveHistoryRef;
using EssPortal.LeaveRef;
using EssPortal.Models;
using EssPortal.PerformanceMgmtRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssPortal.Classes
{
    public class RequestResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }

        public RequestResponse()
        {
            this.Success = false;
            this.ErrorMessage = String.Empty;
        }
    }

    public class SubmitAppraisalResponse : RequestResponse
    {
        public PerformanceMgmt Appraisal { get; set; }
    }

    public class LeaveCreateResponse : RequestResponse
    {
        public string NewLeaveNo { get; set; }

        public LeaveCreateResponse()
        {
            this.NewLeaveNo = String.Empty;
        }
    }

    public class LeaveHistoryResponse : RequestResponse
    {
        public List<LeaveHistory> LeaveHistory { get; set; }

        public LeaveHistoryResponse()
        {
            this.LeaveHistory = new List<LeaveHistoryRef.LeaveHistory>();
        }
    }

    public class JobApplicationResponse : RequestResponse
    {
        public string JobApplicationNo { get; set; }
    }

    public class LeaveResponse : RequestResponse
    {
        public List<Leave> Leave { get; set; }

        public LeaveResponse()
        {
            this.Leave = new List<LeaveRef.Leave>();
        }
    }

    public class IsEmployeeResponse : RequestResponse
    {
        public bool IsEmployee { get; set; }
        public string EmployeeNo { get; set; }
        public string NavUserId { get; set; }
    }

    public class EmployeeDataResponse : RequestResponse
    {
        public Employee Employee { get; set; }
    }

    public class EmployeesDataResponse : RequestResponse
    {
        public List<Employee> Employees { get; set; }

        public EmployeesDataResponse()
        {
            this.Employees = new List<Employee>();
        }
    }

    public class EmployeeAppraisalResponse : RequestResponse
    {
        public List<PerformanceMgmt> Appraisals { get; set; }

        public EmployeeAppraisalResponse()
        {
            Appraisals = new List<PerformanceMgmt>();
        }
    }

    public class AppraisalResponse : RequestResponse
    {
        public PerformanceMgmt Appraisal { get; set; }

        public AppraisalResponse()
        {
            Appraisal = new PerformanceMgmt();
        }
    }

    public class BehaviourTemplateResponse : RequestResponse
    {
        public List<BehaviourTemplateGroup> TemplateGroup { get; set; }

        public BehaviourTemplateResponse()
        {
            TemplateGroup = new List<BehaviourTemplateGroup>();
        }
    }
}