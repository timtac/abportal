﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssPortal.Classes
{
    public class SelectListItemsSelect2
    {
        public string id { get; set; }
        public string text { get; set; }
        public bool Selected { get; set; }
        public string EmployeeNo { get; set; }
    }
}