﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EssPortal.Classes
{
    public class LogUtil
    {
        public string FilePathLOG
        {
            get;
            set;
        }

        public void Logger(String FileName, String ErrorMessage, string extension = ".txt")
        {
            string LogFile = this.FilePathLOG;
            // attempt creating the directory if it does not exist.
            if (!Directory.Exists(LogFile))
            {
                try
                {
                    Directory.CreateDirectory(LogFile);
                }
                catch (Exception)
                {}
            }

            String sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
            String sErrorTime = DateTime.Now.ToString("yyyyMMdd");// sYear + sMonth + sDay;
            try
            {
                using (var sw = new StreamWriter(LogFile + FileName + sErrorTime + extension, true))
                {
                    sw.WriteLine(sLogFormat + ErrorMessage);
                }                
            }
            catch (Exception)
            {
            }
                     
        }
    }
}