﻿using EssPortal.Classes;
using EssPortal.Models;
using EssPortal.PerformanceMgmtRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EssPortal.Controllers
{
    [Authorize(Roles = "Employee")]
    public class AppraisalController : BaseController
    {
        string _APP_NOT_FOUND = "Matching appraisal for request was not found.";
        // GET: Appraisal
        public ActionResult Index()
        {
            AppraisalIndexViewModel model = new AppraisalIndexViewModel();
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            var appraisals = base.Service.GetEmployeeAppraisals(User.Identity.EmployeeNo());
            var pendingApproval = base.Service.GetAppraisalPendingApproval(User.Identity.EmployeeNo()).Appraisals;
            model.Appraisals = appraisals.Appraisals;
            model.PendingApprovals = pendingApproval;
            if (!appraisals.Success)
            {
                Information(appraisals.ErrorMessage, true);
            }
            return View(model);
        }

        public List<BehaviourTemplateGroup> GetBehaviourTemplates(string EmployeeNo)
        {
            base.Init();
            bool hasManagerRole = false;
            var employeeData = base.Service.EmployeeData(EmployeeNo);
            if (employeeData.Success && employeeData.Employee != null)
            {
                hasManagerRole = base.Service.HasManagerialRole(employeeData.Employee.Job_ID);
                var response = base.Service.GetBehaviouralTemplate(hasManagerRole);
                if (response.Success)
                {
                    return response.TemplateGroup;
                }
            }
            return new List<BehaviourTemplateGroup>();
        }

        // Add neccesary initializations to model before returning to the view - Most useful for invalid model situations.
        private void ModelInit(AppraisalViewModel model)
        {
            base.Init();
            var definedRatings = base.Service.PerformaceRatings();
            var rating = base.Service.PerformanceRatingSelectList(""); // TODO LOOK INTO PASSED PARAMETER
            model.DefinedRatings = definedRatings;
            model.Rating = rating;
            foreach (var item in model.PerformanceCriteriaPreCurrYear)
            {
                item.AppraiserAssessmentRating = rating;
                item.SelfAssesmentRating = rating;
            }
            foreach (var item in model.CompetencyCriteriaBehaviour.GroupMembers)
            {
                item.AppraiserAssessmentRating = rating;
                item.SelfAssesmentRating = rating;
            }
        }

        // Create a new appraisal
        public ActionResult SubmitAppraisal()
        {
            AppraisalViewModel model = new AppraisalViewModel();
            base.Init();
            var definedRatings = base.Service.PerformaceRatings();
            var rating = base.Service.PerformanceRatingSelectList(""); // TODO LOOK INTO PASSED PARAMETER
            

            model.Rating = rating;
            model.DefinedRatings = definedRatings;

                      

            model.ObjectiveForNextAppraisal.Add(new PerformanceCriteria());
            model.OpenRoomDiscussion.Add(new PerformanceCriteria() { Objective = "Suggestions about improvements in team/department and/or bank should be discussed" });
            model.OpenRoomDiscussion.Add(new PerformanceCriteria() { Objective = "Optional: What is motivating /demotivating in your work environment?" });
            model.IsAppraiser = false;

            var behaviourTemplates = this.GetBehaviourTemplates(User.Identity.EmployeeNo());

            foreach (var template in behaviourTemplates)
            {
                if (template.GroupName == base.Service.AppraisalCompetencyCriteriaBehaviourKey)
                {
                    model.CompetencyCriteriaBehaviour = template;
                }
                else if (template.GroupName == base.Service.AppraisalSelfAssesementIndividualDevKey)
                {
                    model.SelfAssesementIndividualDev = template;
                }
            }

            // Section  to get data needed to autopopulate the model.
            // Employee and supervisor information
            var employeeNo = this.User.Identity.EmployeeNo();
            var employeeRes = base.Service.EmployeeData(employeeNo);
            var hrSetUp = base.Service.GetHrSetUp();

            

            if (employeeRes.Success && employeeRes.Employee != null)
            {
                var employee = employeeRes.Employee;
                model.DateOfJoining = employee.Date_Of_Joining_the_Company.ToString(DATE_FORMAT);
                model.EmployeeBranch = employee.Global_Dimension_2_Code;
                model.EmployeeName = employee.First_Name + " " + employee.Middle_Name + " " + employee.Last_Name;
                model.EmployeeNo = employee.No;
                model.JobTitle = employee.Job_Title;
                model.AppraisalDate = DateTime.Now.ToString(DATE_FORMAT);


                string appPeriod = !String.IsNullOrWhiteSpace(hrSetUp.Previous_Appraisal_Period) ? hrSetUp.Previous_Appraisal_Period : hrSetUp.Appraisal_Period;
                var previousAppraisal = base.Service.EmployeePreviousYearAppraisal(employee.No, appPeriod);
                
                model.AppraisalPeriod = hrSetUp.Appraisal_Period;                
                model.DateOfLastAppraisal = previousAppraisal != null ? previousAppraisal.Appraisal_Date.ToString(base.DATE_FORMAT) : ""; //base.Service.EmployeeLastAppraisalDate(employee.No, appPeriod);

                
                // Get the performance criteria for the previous year.
                if (String.IsNullOrWhiteSpace(hrSetUp.Previous_Appraisal_Period) || previousAppraisal == null )
                {
                    //  Initialize a blank Performance criteria.
                    model.PerformanceCriteriaPreCurrYear.Add(new PerformanceCriteria() { AppraiserAssessmentRating = rating, SelfAssesmentRating = rating });
                }
                else
                {
                    //foreach (var item in previousAppraisal.HR_Appraisal_Career_Dev_Ques)
                    //{
                    //    model.PerformanceCriteriaPreCurrYear.Add(new PerformanceCriteria
                    //    {
                    //        Description = item.Description,
                    //        Objective = item.Planned_Targets_Objectives,
                    //        AppraiserAssessmentRating = rating,
                    //        SelfAssesmentRating = rating
                    //        //LineNo = item.Line_No
                    //    });
                    //}
                }




                var supervisorRes = base.Service.EmployeeData(employee.Supervisor_ID);
                if (supervisorRes.Success && supervisorRes.Employee != null)
                {
                    var supervisor = supervisorRes.Employee;
                    model.AppraiserJobTitle = supervisor.Job_Title;
                    model.AppraiserName = supervisor.First_Name + " " + supervisor.Middle_Name + " " + supervisor.Last_Name;
                }
                else
                {
                    Information(supervisorRes.ErrorMessage, true);
                }
            }
            else
            {
                Information(employeeRes.ErrorMessage, true);
            }
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            return View(model);
        }

        // Submit a new appraisal.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitAppraisal(AppraisalViewModel model)
        {
            base.Init();
            ModelInit(model);
            ViewBag.IsSupervisor = base.UserIsSupervisor();

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var employeeNo = User.Identity.EmployeeNo();
            string newAppraisalNo = base.Service.InsertAppraisal(employeeNo);

            var appraisalResponse = base.Service.GetAppraisal(newAppraisalNo, employeeNo, "");
            if (appraisalResponse.Success && appraisalResponse.Appraisal != null)
            {
                var modifiedAppraisal = this.UpdateAppraisalSection(appraisalResponse.Appraisal, model);

                // Update the newly created record with other appraisal data.
                List<BehaviourTemplateGroup> templates = new List<BehaviourTemplateGroup>();
                templates.Add(model.CompetencyCriteriaBehaviour);
                templates.Add(model.SelfAssesementIndividualDev);

                // If the button "Send to approver was clicked" the status of the Application is updated and a mail sent to the approver.
                if (model.SendForApproval)
                {
                    appraisalResponse.Appraisal.Status = Status.Pending_Approval;
                    appraisalResponse.Appraisal.StatusSpecified = true;
                }
                base.Service.UpdateAppraisalBehaviourTemplates(appraisalResponse.Appraisal, templates, model.IsAppraiser);
                // Send a mail after the Appraisal has been updated.
                if (model.SendForApproval)
                {
                    base.Service.MailAppraiserNotification(appraisalResponse.Appraisal.Appraisal_No);
                    Information("Thank you for your thoughts and preparing for the appraisal meeting. Your self-assessment has been sent to your supervisor/ appraiser.", true);
                }
                    
                return RedirectToAction("Index");
            }
            else
            {
                string error = String.IsNullOrWhiteSpace(appraisalResponse.ErrorMessage) ? _APP_NOT_FOUND : appraisalResponse.ErrorMessage;
                Danger(error, true);
                return View(model);
            }
        }

        /*// Submit a new appraisal.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitAppraisal(AppraisalViewModel model)
        {
            base.Init();
            ModelInit(model);

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            base.Init();
            PerformanceMgmt appraisal = new PerformanceMgmt();
            //appraisal.Employee_No = User.Identity.EmployeeNo();
            appraisal.User_ID = User.Identity.EmployeeNavUserId();
            appraisal.HRApprais = new HR_Appraisal_Current_KPI[model.PerformanceCriteriaPreCurrYear.Count];
            for (int i = 0; i < model.PerformanceCriteriaPreCurrYear.Count; i++)
            {
                appraisal.HRApprais[i] = new HR_Appraisal_Current_KPI();
                appraisal.HRApprais[i].Description = model.PerformanceCriteriaPreCurrYear[i].Description;
                appraisal.HRApprais[i].Planned_Targets_Objectives = model.PerformanceCriteriaPreCurrYear[i].Objective;
                appraisal.HRApprais[i].Ratings = model.PerformanceCriteriaPreCurrYear[i].SelfAssesment;
                //appraisal.HRApprais[i].Appraiser_Rating = model.PerformanceCriteriaPreCurrYear[i].AppraiserAssessment;
            }

            appraisal.HR_Appraisal_Discussion = new HR_Appraisal_Discussion[model.OpenRoomDiscussion.Count];
            for (int i = 0; i < model.OpenRoomDiscussion.Count; i++)
            {
                appraisal.HR_Appraisal_Discussion[i] = new HR_Appraisal_Discussion();
                appraisal.HR_Appraisal_Discussion[i].Comment = model.OpenRoomDiscussion[i].Objective;
                appraisal.HR_Appraisal_Discussion[i].Appraiser_Remarks = model.OpenRoomDiscussion[i].SelfAssesment;
                //appraisal.HR_Appraisal_Discussion[i].Actual_Results_Manager = model.OpenRoomDiscussion[i].AppraiserAssessment;
            }

            appraisal.HR_Appraisal_Career_Dev_Ques = new HR_Appraisal_Career_Dev_Ques[model.ObjectiveForNextAppraisal.Count];
            for (int i = 0; i < model.ObjectiveForNextAppraisal.Count; i++)
            {
                appraisal.HR_Appraisal_Career_Dev_Ques[i] = new HR_Appraisal_Career_Dev_Ques();
                appraisal.HR_Appraisal_Career_Dev_Ques[i].Description = model.ObjectiveForNextAppraisal[i].Description;
                appraisal.HR_Appraisal_Career_Dev_Ques[i].Planned_Targets_Objectives = model.ObjectiveForNextAppraisal[i].Objective;
            }
            // Create the appraisal record.
            var response = base.Service.SubmitAppraisal(appraisal);

            if (response.Success)
            {
                // Update the newly created record with other appraisal data.
                List<BehaviourTemplateGroup> templates = new List<BehaviourTemplateGroup>();
                templates.Add(model.CompetencyCriteriaBehaviour);
                templates.Add(model.SelfAssesementIndividualDev);

                // If the button "Send to approver was clicked" the status of the Application is updated and a mail sent to the approver.
                if (model.SendForApproval)
                {
                    response.Appraisal.Status = Status.Pending_Approval;
                    response.Appraisal.StatusSpecified = true;
                }
                base.Service.UpdateAppraisalBehaviourTemplates(response.Appraisal, templates, model.IsAppraiser);
                // Send a mail after the Appraisal has been updated.
                if (model.SendForApproval)
                    base.Service.MailAppraiserNotification(response.Appraisal.Appraisal_No);              
                return RedirectToAction("Index");
            }
            else
            {
                Information(response.ErrorMessage, true);
                return View(model);
            }
        }*/

        public ActionResult ViewAppraisal(string AppraisalNo, bool IsAppraiser=false)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            AppraisalViewModel model = new AppraisalViewModel();
            model.IsAppraiser = false;
            AppraisalResponse appraisal = new AppraisalResponse();

            // Determine if it is the appraisee or the appraiser that is requesting to view the Appraisal and pass the appropriate filters.
            if (IsAppraiser)
                appraisal = base.Service.GetAppraisal(AppraisalNo, "", User.Identity.EmployeeNo());
            else
                appraisal = base.Service.GetAppraisal(AppraisalNo, User.Identity.EmployeeNo(), "");

            if (appraisal.Success && appraisal.Appraisal != null)
            {
                model = ModelFromAppraisal(appraisal.Appraisal);
            }
            else
                Danger(appraisal.ErrorMessage, true);

            // Caculate the totals and averages from the model daata.
            ViewBag.ComptencySelfTotal = 0;
            ViewBag.ComptencyAppraiserTotal = 0;
            ViewBag.ComptencySelfAverage = 0;
            ViewBag.ComptencyAppraiserAverage = 0;

            ViewBag.PerformanceSelfTotal = 0;
            ViewBag.PerformanceAppraiserTotal = 0;
            ViewBag.PerformanceSelfAverage = 0;
            ViewBag.PerformanceAppraiserAverage = 0;           

            if (model.CompetencyCriteriaBehaviour.GroupMembers.Count > 0)
            {
                var count = model.CompetencyCriteriaBehaviour.GroupMembers.Count;
                decimal Selftotal = 0, AppraiserTotal = 0;
                
                foreach (var item in model.CompetencyCriteriaBehaviour.GroupMembers)
                {
                    Selftotal += item.SelfRatingValue;
                    AppraiserTotal += item.AppraiserRatingValue;
                }

                ViewBag.ComptencySelfTotal = Selftotal;
                ViewBag.ComptencyAppraiserTotal = AppraiserTotal;
                ViewBag.ComptencySelfAverage = (Selftotal / count).ToString("#.####");
                ViewBag.ComptencyAppraiserAverage = (AppraiserTotal / count).ToString("#.####");
            }

            if (model.PerformanceCriteriaPreCurrYear.Count > 0)
            {
                var count = model.PerformanceCriteriaPreCurrYear.Count;
                decimal Selftotal = 0, AppraiserTotal = 0;

                foreach (var item in model.PerformanceCriteriaPreCurrYear)
                {
                    Selftotal += item.SelfRatingValue;
                    AppraiserTotal += item.AppraiserRatingValue;
                }

                ViewBag.PerformanceSelfTotal = Selftotal;
                ViewBag.PerformanceAppraiserTotal = AppraiserTotal;
                ViewBag.PerformanceSelfAverage = (Selftotal / count).ToString("#.####");
                ViewBag.PerformanceAppraiserAverage = (AppraiserTotal / count).ToString("#.####");
            }

            return View(model);
        }

        // Make a model from incoming Appraisal data.
        public AppraisalViewModel ModelFromAppraisal(PerformanceMgmt Appraisal)
        {
            AppraisalViewModel model = new AppraisalViewModel();
            model.AppraisalNo = Appraisal.Appraisal_No;
            model.AppraisalPeriod = Appraisal.Appraisal_Period;
            model.EmployeeName = Appraisal.Employee_Name;
            model.EmployeeNo = Appraisal.Employee_No;
            model.JobTitle = Appraisal.Job_Title;
            model.EmployeeBranch = Appraisal.Global_Dimension_2_Code;
            model.DateOfJoining = Appraisal.Employment_Date.ToString(base.DATE_FORMAT);
            model.DateOfLastAppraisal = Appraisal.Date_of_Last_Review.ToString(base.DATE_FORMAT);
            model.AppraiserName = Appraisal.Supervisor;
            model.AppraiserJobTitle = Appraisal.Supervisors_Job_Title;
            model.AppraisalDate = Appraisal.Appraisal_Date.ToString(base.DATE_FORMAT);
            model.AppraisalTalkTookPlace = Appraisal.Appraisal_Talk_Took_Place.ToString();
            model.DateOfAgreement = Utils.DateDisplayString(Appraisal.Appraisal_Talk_Took_Place_on);

            //model.TaskObjective
            base.Init();

            // Get the behaviural template to prepopulate each section
            var behaviourTemplates = this.GetBehaviourTemplates(Appraisal.Employee_No);
            BehaviourTemplateGroup competencyGroup = new BehaviourTemplateGroup();
            BehaviourTemplateGroup generalGroup = new BehaviourTemplateGroup();

            foreach (var template in behaviourTemplates)
            {
                if (template.GroupName == base.Service.AppraisalCompetencyCriteriaBehaviourKey)
                {
                    competencyGroup = template;
                }
                else if (template.GroupName == base.Service.AppraisalSelfAssesementIndividualDevKey)
                {
                    generalGroup = template;
                }
            }

            var definedRatings = base.Service.PerformaceRatings();
            var rating = base.Service.PerformanceRatingSelectList(""); // TODO LOOK INTO PASSED PARAMETER
            model.Rating = rating;
            model.DefinedRatings = definedRatings;
            model.PerformanceCriteriaPreCurrYear = new List<PerformanceCriteria>();
            foreach (var item in Appraisal.HRApprais)
            {
                model.PerformanceCriteriaPreCurrYear.Add(new PerformanceCriteria()
                {
                    Description = item.Description,
                    Objective = item.Planned_Targets_Objectives,
                    SelfAssesment = item.Ratings,
                    SelfRatingValue = item.Actual_Results_Self,
                    AppraiserAssessment = item.Appraiser_Rating,
                    AppraiserRatingValue = item.Agreed_Score,
                    LineNo = item.Line_No,
                    AppraiserAssessmentRating = rating,
                    SelfAssesmentRating = rating,
                    AppraiseeComment = item.Appraisee_Comment,
                    AppraiserComment = item.Appraiser_Comment
                });
            }

            // Get all data related to competency and behavioural.
            model.CompetencyCriteriaBehaviour = new BehaviourTemplateGroup();
            model.CompetencyCriteriaBehaviour.GroupName = base.Service.AppraisalCompetencyCriteriaBehaviourKey;
            model.CompetencyCriteriaBehaviour.Rating = rating;
            model.CompetencyCriteriaBehaviour.GroupMembers = new List<PerformanceCriteria>();
            foreach (var item in Appraisal.HR_Appraisal_Behavioural_Tab)
            {
                if (competencyGroup.GroupMembers.Where(a => a.Objective == item.Planned_Targets_Objectives).Count() > 0)
                {
                    model.CompetencyCriteriaBehaviour.GroupMembers.Add(new PerformanceCriteria
                    {
                        Objective = item.Planned_Targets_Objectives,
                        Description = item.Description,
                        Description_2 = item.Description_2,
                        AppraiserAssessment = item.Appraiser_Rating,
                        SelfAssesment = item.Ratings,
                        SelfRatingValue = item.Actual_Results_Self,
                        AppraiserAssessmentRating = rating,
                        AppraiserRatingValue = item.Agreed_Score,
                        SelfAssesmentRating = rating,
                        AppraiseeComment = item.Appraisee_Comment,
                        AppraiserComment = item.Appraiser_Comment
                    });
                }                
            }

            // Get all data related to Self Assessment template.
            model.SelfAssesementIndividualDev = new BehaviourTemplateGroup();
            model.SelfAssesementIndividualDev.GroupName = base.Service.AppraisalSelfAssesementIndividualDevKey;
            model.SelfAssesementIndividualDev.Rating = rating;
            model.SelfAssesementIndividualDev.GroupMembers = new List<PerformanceCriteria>();
            foreach (var item in Appraisal.HR_Appraisal_General_Assessmen)
            {
                model.SelfAssesementIndividualDev.GroupMembers.Add(new PerformanceCriteria
                {
                    Objective = item.Planned_Targets_Objectives,
                    AppraiserAssessment = item.Actual_Results_Manager,
                    SelfAssesment = item.Appraiser_Comment
                });
            }

            // Data for next appraisal objective.
            model.ObjectiveForNextAppraisal = new List<PerformanceCriteria>();
            foreach (var item in Appraisal.HR_Appraisal_Career_Dev_Ques)
            {
                model.ObjectiveForNextAppraisal.Add(new PerformanceCriteria
                {
                    Description = item.Description,
                    Objective = item.Planned_Targets_Objectives,
                    LineNo = item.Line_No
                });
            }

            // Data for open room discussion
            model.OpenRoomDiscussion = new List<PerformanceCriteria>();
            foreach (var item in Appraisal.HR_Appraisal_Discussion)
            {
                model.OpenRoomDiscussion.Add(new PerformanceCriteria
                {
                    Objective = item.Comment,
                    SelfAssesment = item.Appraiser_Comment,
                    AppraiserAssessment = item.Actual_Results_Manager,
                    LineNo = item.Line_No
                });
            }

            return model;
        }

        // Update sections of an appraisal based on wether the logged submition is from an appraisee or an appraiser
        // retrun the updated appraisal.
        private PerformanceMgmt UpdateAppraisalSection(PerformanceMgmt Appraisal, AppraisalViewModel model)
        {
            #region .................... Previous current year performance criteria ..........................

            // Delete related appraisal carrer dev questions and Objective
            base.Init();
                var deleted = base.Service.DeleteAppraisalObjectiveAndPrevCurrYearPerCriteria(Appraisal.Appraisal_No, Appraisal.Appraisal_Period);

            // Add new appraisal carrer dev questions and Objective
            if (deleted.Success)
            {
                List<HR_Appraisal_Current_KPI> newHrAppraise = new List<HR_Appraisal_Current_KPI>();
                // Create the new entries
                foreach (var entry in model.PerformanceCriteriaPreCurrYear)
                {
                    HR_Appraisal_Current_KPI newEntry = new HR_Appraisal_Current_KPI();
                    if (model.IsAppraiser)
                    {
                        //newEntry.Appraiser_Rating = entry.AppraiserAssessment;
                        //newEntry.Appraiser_Comment = entry.AppraiserComment;
                    }
                    else
                    {
                        //newEntry.Description = entry.Description;
                        //newEntry.Planned_Targets_Objectives = entry.Objective;
                        //newEntry.Ratings = entry.SelfAssesment;
                        //newEntry.Appraisee_Comment = entry.AppraiseeComment;
                    }
                    newEntry.Appraiser_Rating = entry.AppraiserAssessment;
                    newEntry.Appraiser_Comment = entry.AppraiserComment;
                    newEntry.Description = entry.Description;
                    newEntry.Planned_Targets_Objectives = entry.Objective;
                    newEntry.Ratings = entry.SelfAssesment;
                    newEntry.Appraisee_Comment = entry.AppraiseeComment;
                    newHrAppraise.Add(newEntry);
                }
                Appraisal.HRApprais = new HR_Appraisal_Current_KPI[newHrAppraise.Count];
                Appraisal.HRApprais = newHrAppraise.ToArray();

                // Add the carrer dev question
                List<HR_Appraisal_Career_Dev_Ques> newCarrerDev = new List<HR_Appraisal_Career_Dev_Ques>();

                foreach (var entry in model.ObjectiveForNextAppraisal)
                {
                    HR_Appraisal_Career_Dev_Ques newEntry = new HR_Appraisal_Career_Dev_Ques();
                   
                    newEntry.Description = entry.Description;
                    newEntry.Planned_Targets_Objectives = entry.Objective;
                    
                    newCarrerDev.Add(newEntry);
                }
                Appraisal.HR_Appraisal_Career_Dev_Ques = new HR_Appraisal_Career_Dev_Ques[newCarrerDev.Count];
                Appraisal.HR_Appraisal_Career_Dev_Ques = newCarrerDev.ToArray();
            }
            else
            {
                //List<HR_Appraisal_Current_KPI> hrAppraise = new List<HR_Appraisal_Current_KPI>();
                //List<HR_Appraisal_Current_KPI> newHrAppraise = new List<HR_Appraisal_Current_KPI>();
                //hrAppraise.AddRange(Appraisal.HRApprais);
                //foreach (var entry in model.PerformanceCriteriaPreCurrYear)
                //{
                //    // Assumption is if the line cannot be found, then it is a new record altogether. otherwise update the old record.
                //    var searchResult = hrAppraise.Find(a => a.Line_No == entry.LineNo);
                //    if (searchResult != null)
                //    {
                //        if (model.IsAppraiser)
                //        {
                //            searchResult.Appraiser_Rating = entry.AppraiserAssessment;
                //            searchResult.Appraiser_Comment = entry.AppraiserComment;
                //        }
                //        else
                //        {
                //            searchResult.Description = entry.Description;
                //            searchResult.Planned_Targets_Objectives = entry.Objective;
                //            searchResult.Ratings = entry.SelfAssesment;
                //            searchResult.Appraisee_Comment = entry.AppraiseeComment;
                //        }
                //        newHrAppraise.Add(searchResult);
                //    }
                //    else
                //    {
                //        HR_Appraisal_Current_KPI newEntry = new HR_Appraisal_Current_KPI();
                //        if (model.IsAppraiser)
                //        {
                //            newEntry.Appraiser_Rating = entry.AppraiserAssessment;
                //            newEntry.Appraiser_Comment = entry.AppraiserComment;
                //        }
                //        else
                //        {
                //            newEntry.Description = entry.Description;
                //            newEntry.Planned_Targets_Objectives = entry.Objective;
                //            newEntry.Ratings = entry.SelfAssesment;
                //            newEntry.Appraisee_Comment = entry.AppraiseeComment;
                //        }
                //        newHrAppraise.Add(newEntry);
                //    }
                //}
                //Appraisal.HRApprais = new HR_Appraisal_Current_KPI[newHrAppraise.Count];
                //Appraisal.HRApprais = newHrAppraise.ToArray();
            }

            #endregion

            #region ............. Update Discussion section of the appraisal .........................
            List<HR_Appraisal_Discussion> disc = new List<HR_Appraisal_Discussion>();
            List<HR_Appraisal_Discussion> newDis = new List<HR_Appraisal_Discussion>();
            disc.AddRange(Appraisal.HR_Appraisal_Discussion);
            foreach (var entry in model.OpenRoomDiscussion)
            {
                // Assumption is if the line cannot be found, then it is a new record altogether. otherwise update the old record.
                var searchResult = disc.Find(a => a.Line_No == entry.LineNo);
                if (searchResult != null)
                {
                    if (model.IsAppraiser)
                    {
                        searchResult.Actual_Results_Manager = entry.AppraiserAssessment;
                    }
                    else
                    {
                        searchResult.Comment = entry.Objective;
                        searchResult.Appraiser_Comment = entry.SelfAssesment;
                    }
                    newDis.Add(searchResult);
                }
                else
                {
                    HR_Appraisal_Discussion newEntry = new HR_Appraisal_Discussion();
                    if (model.IsAppraiser)
                    {
                        newEntry.Actual_Results_Manager = entry.AppraiserAssessment;
                    }
                    else
                    {
                        newEntry.Comment = entry.Objective;
                        newEntry.Appraiser_Comment = entry.SelfAssesment;
                    }
                    newDis.Add(newEntry);
                }
            }
            Appraisal.HR_Appraisal_Discussion = new HR_Appraisal_Discussion[newDis.Count];
            Appraisal.HR_Appraisal_Discussion = newDis.ToArray();
            #endregion

            #region ........ Update carrer development section of the appraisal..................

            //List<HR_Appraisal_Career_Dev_Ques> careerDev = new List<HR_Appraisal_Career_Dev_Ques>();
            //List<HR_Appraisal_Career_Dev_Ques> newCarrerDev = new List<HR_Appraisal_Career_Dev_Ques>();
            //careerDev.AddRange(Appraisal.HR_Appraisal_Career_Dev_Ques);
            //foreach (var entry in model.ObjectiveForNextAppraisal)
            //{
            //    // Assumption is if the line cannot be found, then it is a new record altogether. otherwise update the old record.
            //    var searchResult = careerDev.Find(a => a.Line_No == entry.LineNo);
            //    if (searchResult != null)
            //    {
            //        if (!model.IsAppraiser)
            //        {
            //            searchResult.Description = entry.Description;
            //            searchResult.Planned_Targets_Objectives = entry.Objective;
            //        }
            //        newCarrerDev.Add(searchResult);
            //    }
            //    else
            //    {
            //        HR_Appraisal_Career_Dev_Ques newEntry = new HR_Appraisal_Career_Dev_Ques();
            //        if (!model.IsAppraiser)
            //        {
            //            newEntry.Description = entry.Description;
            //            newEntry.Planned_Targets_Objectives = entry.Objective;
            //        }
            //        newCarrerDev.Add(newEntry);
            //    }
            //}
            //Appraisal.HR_Appraisal_Career_Dev_Ques = new HR_Appraisal_Career_Dev_Ques[newCarrerDev.Count];
            //Appraisal.HR_Appraisal_Career_Dev_Ques = newCarrerDev.ToArray();
            #endregion

            return Appraisal;
        }

        // Appraisee reviewing previously completed application
        public ActionResult ReviewAppraisal(string AppraisalNo)
        {
            base.Init();
            AppraisalViewModel model = new AppraisalViewModel();
            model.IsAppraiser = false;
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            var appraisal = base.Service.GetAppraisal(AppraisalNo, User.Identity.EmployeeNo(), "");
            if (appraisal.Success && appraisal.Appraisal != null)
            {
                if (appraisal.Appraisal.Appraisal_Period_Closed)
                    Information("The appraisal period has been closed.", true);
                else
                {
                    model = ModelFromAppraisal(appraisal.Appraisal);
                    return View(model);
                }                
            }
            else
                Danger("The requested appraisal was not found.", true);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReviewAppraisal(AppraisalViewModel model, string AppraisalNo)
        {
            base.Init();
            // Reattach neccesary initilizations parameters to the model.
            ModelInit(model);
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var appraisal = base.Service.GetAppraisal(model.AppraisalNo, User.Identity.EmployeeNo(), "");
            if (appraisal.Success && appraisal.Appraisal != null)
            {
                if (model.SendForApproval)
                {
                    appraisal.Appraisal.Status = Status.Pending_Approval;
                    appraisal.Appraisal.StatusSpecified = true;
                }
                var modifiedAppraisal = this.UpdateAppraisalSection(appraisal.Appraisal, model);
                //this.Service.UpdateAppraisal(modifiedAppraisal);

                // Update the record with other Competency and self assesment data.
                List<BehaviourTemplateGroup> templates = new List<BehaviourTemplateGroup>();
                templates.Add(model.CompetencyCriteriaBehaviour);
                templates.Add(model.SelfAssesementIndividualDev);
                base.Service.UpdateAppraisalBehaviourTemplates(modifiedAppraisal, templates, model.IsAppraiser);

                if (model.SendForApproval)
                {
                    base.Service.MailAppraiserNotification(appraisal.Appraisal.Appraisal_No);
                    Information("Thank you for your thoughts and preparing for the appraisal meeting. Your self-assessment has been sent to your supervisor/ appraiser.", true);
                }

                return RedirectToAction("Index");
            }
            return View(model);
        }

        // Appraiser reviewing previously completed application
        // Use the AppraiseePortalUserId to retrieve the employeeNo of the appraisee.
        public ActionResult AppraiserReviewAppraisal(string AppraisalNo, string AppraiseePortalUserId)
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            AppraisalViewModel model = new AppraisalViewModel();
            var appraisal = base.Service.GetAppraisal(AppraisalNo, "", User.Identity.EmployeeNo());
            if (appraisal.Success && appraisal.Appraisal != null)
            {
                if (appraisal.Appraisal.Appraisal_Period_Closed)
                {
                    Information("The appraisal period has been closed.", true);
                    return RedirectToAction("Index");
                }
                else
                {
                    model = ModelFromAppraisal(appraisal.Appraisal);
                    model.IsAppraiser = true;
                    return View(model);
                }
            }
            string error = String.IsNullOrWhiteSpace(appraisal.ErrorMessage) ? _APP_NOT_FOUND : appraisal.ErrorMessage;
            Danger(error, true);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AppraiserReviewAppraisal(AppraisalViewModel model, string AppraisalNo)
        {
            base.Init();
            ModelInit(model);
            ViewBag.IsSupervisor = base.UserIsSupervisor();

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var appraisal = base.Service.GetAppraisal(model.AppraisalNo, "", User.Identity.EmployeeNo());
            if (appraisal.Success && appraisal.Appraisal != null)
            {
                // IF THE APPRAISER CLICKED THE SAVE BUTTON, DONT MODIFY THE STATUS OF THE APRAISAL.
                if (!model.ApproverSave)
                {
                    if (model.SendToHr)
                    {
                        appraisal.Appraisal.Status = Status.Approved;
                        appraisal.Appraisal.Appraisal_Talk_Took_Place_on = Service.StringToDate(model.DateOfAgreement);
                        appraisal.Appraisal.Appraisal_Talk_Took_Place_onSpecified = true;
                        appraisal.Appraisal.Appraisal_Talk_Took_Place = (Appraisal_Talk_Took_Place)Enum.Parse(typeof(Appraisal_Talk_Took_Place),model.AppraisalTalkTookPlace);
                        appraisal.Appraisal.Appraisal_Talk_Took_PlaceSpecified = true;
                        Information("Thank you for the completion of the appraisal. Your result sheet has been sent to the HR team.");
                    }
                    else
                    {
                        // Send the appraisal back to the appraisee
                        appraisal.Appraisal.Status = Status.Appraisee_Review;
                    }
                    appraisal.Appraisal.StatusSpecified = true;
                }

                var modifiedAppraisal = this.UpdateAppraisalSection(appraisal.Appraisal, model);

                // Update the record with other Competency and self assesment data.
                List<BehaviourTemplateGroup> templates = new List<BehaviourTemplateGroup>();
                templates.Add(model.CompetencyCriteriaBehaviour);
                templates.Add(model.SelfAssesementIndividualDev);
                base.Service.UpdateAppraisalBehaviourTemplates(modifiedAppraisal, templates, model.IsAppraiser);

                // Send neccessary mails after the appraisal has been updated
                if (!model.ApproverSave)
                {
                    if (model.SendToHr)
                    {
                        Service.MailNotifyHrOfCompletedAppraisal(model.AppraisalNo);
                    }
                    else
                    {
                        Service.MailNotifyAppraisee(model.AppraisalNo);
                    }
                }
                return RedirectToAction("Index");
            }
            string error = String.IsNullOrWhiteSpace(appraisal.ErrorMessage) ? _APP_NOT_FOUND : appraisal.ErrorMessage;
            Danger(error, true);
            return View(model);
        }

        public ActionResult SupervisorApprovedAppraisal()
        {
            AppraisalIndexViewModel model = new AppraisalIndexViewModel();
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();

            // Get all the appraisal that has been approved by the currently logged in Supervisor.
            var appraisals = base.Service.GetAppraisalPendingApproval(User.Identity.EmployeeNo(), true);

            // Get all the appraisal that are pending approval by the currently logged in Supervisor.
            var pendingApproval = base.Service.GetAppraisalPendingApproval(User.Identity.EmployeeNo()).Appraisals;

            model.Appraisals = appraisals.Appraisals;
            model.PendingApprovals = pendingApproval;
            if (!appraisals.Success)
            {
                Danger(appraisals.ErrorMessage, true);
            }
            return View(model);
        }
    }
}