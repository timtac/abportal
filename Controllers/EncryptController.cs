﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using EssPortal.Models;

namespace EssPortal.Controllers
{
    [Authorize(Roles = "Employee")]
    public class EncryptController : BaseController
    {

        public EncryptController() { }

        
        //GET: Encrypt
        public ActionResult Index()
        {
            base.Init();
            ViewBag.IsSupervisor = base.UserIsSupervisor();
            ViewBag.Title = "Encrpyt";
            EncryptViewModel model = new EncryptViewModel();
            return View(model);
        }

        public JsonResult EncryptText(string PlainText)
        {
            Classes.TextEncryption encryption = new Classes.TextEncryption();
            var encryptedText = encryption.Encrypt(PlainText);

            return Json(encryptedText);
        }
    }
}